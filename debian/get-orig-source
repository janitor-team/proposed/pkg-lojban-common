#! /bin/bash

# debian/get-orig-source
# Part of the ‘lojban-common’ package.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

set -o errexit
set -o errtrace
set -o pipefail
set -o nounset

PACKAGE_NAME=lojban-common
PACKAGE_VERSION=$(
        dpkg-parsechangelog --count 1 \
        | grep '^Version: ' \
        | cut -d ' ' -f 2 \
        | sed -e 's/-[^-]\+$//' \
        )

LOJBAN_FILES_URL=https://www.lojban.org/publications
upstream_urls=(
        ${LOJBAN_FILES_URL}/wordlists/gismu.txt
        ${LOJBAN_FILES_URL}/wordlists/cmavo.txt
        ${LOJBAN_FILES_URL}/wordlists/rafsi.txt
        ${LOJBAN_FILES_URL}/wordlists/lujvo.txt
        )

working_root="$(mktemp -d -t)"

exit_sigspecs="ERR EXIT SIGTERM SIGHUP SIGINT SIGQUIT"

function cleanup_exit() {
    exit_status=$?
    trap - $exit_sigspecs

    rm -rf "${working_root}"
    printf "Cleaned up working directory ‘%s’\n" "${working_root}"

    exit $exit_status
}
trap cleanup_exit $exit_sigspecs

function command_not_found_handle() {
    # Bash will (as per ‘bash(1)’ § “COMMAND EXECUTION”) invoke this
    # function on failure of a search through PATH for a command.
    local command="$1"
    local -a command_args="$@"

    case "$command" in
      wget | /usr/bin/wget)
        printf \
                "This program depends on the program ‘%s’, installed from the Debian package ‘%s’.\n" \
                "wget" "wget"
        ;;
      *)
        ;;
    esac

    printf "%s: %s: command not found\n" "$SHELL" "$command"
    return 127
}

program_dir="$(dirname $(readlink --canonicalize-existing $0))"

upstream_archive_name=${PACKAGE_NAME}-${PACKAGE_VERSION}
archive_working_dir=${working_root}/${upstream_archive_name}
mkdir ${archive_working_dir}

WGET_OPTS="--timestamping --directory-prefix ${archive_working_dir}"
wget ${WGET_OPTS} "${upstream_urls[@]}"

sha1sums_file="${program_dir}/upstream.sha1sums"
(
    cd ${archive_working_dir}
    sha1sum -c "${sha1sums_file}"
    )

TARBALL_SUFFIX=".tar.gz"
tarball_name=${PACKAGE_NAME}_${PACKAGE_VERSION}.orig${TARBALL_SUFFIX}
(
    cd ${working_root}
    tar -cf - ${upstream_archive_name}
    ) | gzip --best > ${tarball_name}


# Copyright © 2005–2022 Ben Finney <bignose@debian.org>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied. See the file ‘LICENSE.GPL-3’ for details.

# Local variables:
# coding: utf-8
# mode: sh
# End:
# vim: fileencoding=utf-8 filetype=sh :
